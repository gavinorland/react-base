# What is it?

A master-project where I keep an optimised Webpack config and library and everything necessary to develop a React project without using [create-react-app](https://github.com/facebookincubator/create-react-app).

# Use case

Other projects should fork this project and pull from it to update.

# Detailed notes

## ES6 and JSX

ES6 is transpiled as part of the Webpack build by [babel-loader](https://github.com/babel/babel-loader). Presets are installed for Babel to be able to convert [stage-0](https://babeljs.io/docs/plugins/preset-stage-0/) syntax and convert JSX into `createElement()` calls.

## JavaScript linting

_Production and development_

This is carried out by way of the Webpack ESLint loader. Why not on a commit hook? Because this would rewrite whole files, making it [impossible](https://github.com/okonet/lint-staged/issues/62) to commit only lines.

ESLint is using [eslint-config-airbnb](https://www.npmjs.com/package/eslint-config-airbnb), which include special preferences, import/export linting and JSX linting.

> Both ESLint and Prettier could also be run via editor extension on save, for instant correction locally, but it's best to keep the Webpack loader too for pipeline certainty since the Git hook is not being used.

## Prettier

Prettier is run along with ESLint via [eslint-plugin-prettier](https://github.com/prettier/eslint-plugin-prettier). So this is done when files change, on each Webpack bundle recompile.

[eslint-config-prettier](https://github.com/prettier/eslint-config-prettier) is installed and its rules specified in `.eslintrc` to turn off any ESLint rules which might conflict with Prettier. [eslint-plugin-flowtype](https://github.com/gajus/eslint-plugin-flowtype) rules are also overruled by extending `"prettier/flowtype"` inside `.eslintrc`.

Prettier itself is configured to use Facebook rules, also in `.eslintrc` - for example trailing commas and no object spacing.

Prettier CLI can also be run via npm script, and it _is_ run like this when the dev server starts, because this way it can catch and format files that are not included in the build process: tests and root configuration `json` files.

## CSS linting

_Development only_

This is again carried out via Webpack on the completed bundle using [stylelint-webpack-plugin](https://github.com/JaKXz/stylelint-webpack-plugin). This has [stylelint-order](https://github.com/hudochenkov/stylelint-order) (alphabetical) configured among its rules in `.stylelintrc.json`. Stylelint will fix what it can on the fly, but it cannot fix everything and the build will fail with errors if there are any.

### Styled Components

`stylelint-processor-styled-components` processor is installed and set in `.stylelintrc.json`. This extracts styles from styled components. `stylelint-config-styled-components` is also set to disable rules which class with `styled-components`.

## Flow

ESLint also lints Flow annotation, thanks to [eslint-plugin-flowtype](https://www.npmjs.com/package/eslint-plugin-flowtype) which is installed and specified in its config file`.eslintrc.json`.

Flow is actually _run_ using [flow-babel-webpack-plugin](https://www.npmjs.com/package/flow-babel-webpack-plugin), which is added via webpack-lib as a plugin to operate on the bundle on each build. This is apparently a plugin to the Babel process.

Flow annotation is removed from the files by Babel due to [babel-preset-react](https://babeljs.io/docs/plugins/preset-react/) which is installed specified in `.babelrc`.

## Jest

Jest is run only for staged files and only tests which relate to those files are run, pre-commit. The commit fails if the tests fail. An npm script runs _all_ tests. Jest also installs [babel-jest](https://github.com/babel/babel-jest). It uses this to be able to understand ES6 and React-specific syntax in your tests, but when it runs Babel it sets the environment variable to `test`, therefore it needs presets in that section in `.babelrc`.
