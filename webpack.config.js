const path = require('path');
const merge = require('webpack-merge');
const lib = require('webpack-lib');

const PATHS = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist'),
};

const commonConfig = merge([
  {
    entry: {
      app: PATHS.src,
    },
    output: {
      path: PATHS.dist,
      filename: '[name].js',
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
  },
  lib.htmlWebpackPlugin({template: 'src/index.pug'}),
  lib.loadJavaScript({include: PATHS.src}),
  lib.loadPug(),
  lib.lintJavaScript({include: PATHS.src, options: {fix: true}}),
  lib.lintStyledComponents({include: PATHS.src}),
]);

const productionConfig = merge([
  lib.clean('dist', {root: __dirname}),
  lib.generateSourceMaps({type: 'source-map'}),
  lib.extractBundles([
    {
      name: 'vendor',
      minChunks: ({resource}) => /node_modules/.test(resource),
    },
    {
      name: 'manifest',
      minChunks: Infinity,
    },
  ]),
  lib.minifyJavaScript({compress: {drop_console: true, reduce_vars: false}}),
  lib.extractCSS({
    use: ['css-loader', lib.autoprefix()],
  }),
  lib.minifyCSS({
    options: {
      discardComments: {
        removeAll: true,
      },
      // Run cssnano in safe mode to avoid
      // potentially unsafe transformations.
      safe: true,
    },
  }),
  lib.setFreeVariable('process.env.NODE_ENV', 'production'),
]);

const developmentConfig = merge([
  lib.devServer({hot: true}),
  lib.loadCSS(),
  lib.lintCSS({fix: true}),
  lib.runFlow(),
  lib.generateSourceMaps({type: 'source-map'}),
  lib.setFreeVariable('process.env.NODE_ENV', 'development'),
]);

module.exports = env => {
  if (env === 'production') {
    return merge(commonConfig, productionConfig);
  }
  return merge(commonConfig, developmentConfig);
};
