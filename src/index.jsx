import React from 'react';
import ReactDOM from 'react-dom';
import Greeting from './Greeting';

ReactDOM.render(<Greeting name="world" />, document.getElementById('root'));
