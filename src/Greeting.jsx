// @flow

import * as React from 'react';
import {hot} from 'react-hot-loader';
import Message from './Message';

type Props = {name: string, children?: React.Node};

const Greeting = (props: Props) => (
  <div>
    {props.children} Hello {props.name}!<Message />
  </div>
);

Greeting.defaultProps = {children: null};

export default hot(module)(Greeting);
